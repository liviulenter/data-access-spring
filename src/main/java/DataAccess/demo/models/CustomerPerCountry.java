package DataAccess.demo.models;

public class CustomerPerCountry {
    private int customers;
    private String country;

    public CustomerPerCountry(int customers, String country) {
        this.customers = customers;
        this.country = country;
    }

    // getters and setters:
    public int getCustomers() {
        return customers;
    }
    public void setCustomers(int customers) {
        this.customers = customers;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
}

