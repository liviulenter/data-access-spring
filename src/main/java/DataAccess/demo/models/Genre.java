package DataAccess.demo.models;

public class Genre {

    private int GenreId;
    private String Name;

    public Genre(int genreId, String name) {
        GenreId = genreId;
        Name = name;
    }
    //getter:
    public String getName() {
        return Name;
    }
}
