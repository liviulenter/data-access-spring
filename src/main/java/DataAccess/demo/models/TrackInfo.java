package DataAccess.demo.models;

public class TrackInfo {

    private  int TrackId;
    private  String TrackName;
    private  String ArtistName;
    private  String AlbumTitle;
    private  String Genre;

    public TrackInfo(int trackId, String trackName, String artistName, String albumTitle, String genre) {
        TrackId = trackId;
        TrackName = trackName;
        ArtistName = artistName;
        AlbumTitle = albumTitle;
        Genre = genre;
    }

    //getters and setters:
    public int getTrackId() { return TrackId; }
    public String getTrackName() {
        return TrackName;
    }
    public String getArtistName() {
        return ArtistName;
    }
    public String getAlbumTitle() { return AlbumTitle; }
    public String getGenre() {
        return Genre;
    }


    public void setTrackId(int trackId) {
        TrackId = trackId;
    }
    public void setTrackName(String trackName) {
        TrackName = trackName;
    }
    public void setArtistName(String artistName) {
        ArtistName = artistName;
    }
    public void setAlbumTitle(String albumTitle) { AlbumTitle = albumTitle; }
    public void setGenre(String genre) {
        Genre = genre;
    }
}
