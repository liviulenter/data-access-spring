package DataAccess.demo.models;

public class HighestSpenders {

    private String CustomerId;
    private String FirstName;
    private String LastName;
    private int Total;

    public String getCustomerId() {
        return CustomerId;
    }
    public String getFirstName() {
        return FirstName;
    }
    public String getLastName() {
        return LastName;
    }
    public int getTotal() {
        return Total;
    }

    public HighestSpenders(String customerId, String firstName, String lastName, int total) {
        CustomerId = customerId;
        FirstName = firstName;
        LastName = lastName;
        Total = total;
    }
}
