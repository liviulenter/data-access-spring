package DataAccess.demo.models;

public class Track {
    private  int TrackId;
    private  String Name;

    public Track(int trackId, String name) {
        TrackId = trackId;
        Name = name;
    }

    //getters and setters:
    public int getTrackId() {
        return TrackId;
    }
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
    public void setTrackId(int trackId) {
        TrackId = trackId;
    }
}
