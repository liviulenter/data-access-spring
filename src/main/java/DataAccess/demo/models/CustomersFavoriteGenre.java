package DataAccess.demo.models;

public class CustomersFavoriteGenre {

    private String firstName;
    private String lastName;
    private String genreType;
    private int genreCount;

    public CustomersFavoriteGenre(String firstName, String lastName, String genreType, int genreCount) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.genreType = genreType;
        this.genreCount = genreCount;
    }
    //getters and setters:
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getGenreType() {
        return genreType;
    }
    public int getGenreCount() {
        return genreCount;
    }


}
