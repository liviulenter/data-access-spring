package DataAccess.demo.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

//this is for logging the date with all the console messages

public class log {
    public static void print(String message) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = new Date();
        System.out.println(dateFormat.format(date) + "  " + message);
    }
}
