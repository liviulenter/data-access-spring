package DataAccess.demo.repositories;


import DataAccess.demo.log.log;
import DataAccess.demo.models.TrackInfo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackInfoRepository {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    private static Connection conn = null;


    /**
     * Gets track info
     *
     * @param searchTerm - search Term
     * @return everything that matches the search term.
     */


    public static ArrayList<TrackInfo> getTrackInfo(String searchTerm) {
        ArrayList<TrackInfo> trackInfoArrayList = new ArrayList<>();

        //if a person searches for nothing, they get nothing
        if (searchTerm == ""){
            log.print("Searching for nothing,you must have input!");
            return trackInfoArrayList;
        }

        try {
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");

            PreparedStatement ps = conn.prepareStatement("SELECT Tr.TrackId, Tr.Name AS Track," +
                    " Ar.Name AS Artist, Al.Title as Album, " +
                    " Ge.Name AS Genre " +
                    " FROM Track Tr " +
                    " INNER JOIN Album Al ON Tr.AlbumId = Al.AlbumId " +
                    " INNER JOIN Artist Ar ON Al.ArtistId= Ar.ArtistId " +
                    " INNER JOIN Genre Ge ON Tr.GenreId= Ge.GenreId " +
                    " WHERE Tr.Name LIKE ?");

            ps.setString(1, "%" + searchTerm + "%");

            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                trackInfoArrayList.add(
                        new TrackInfo(
                                resultSet.getInt("TrackId"),
                                resultSet.getString("Track"),
                                resultSet.getString("Artist"),
                                resultSet.getString("Album"),
                                resultSet.getString("Genre")
                        )
                );
            }
        } catch (Exception e) {
            log.print(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
        return trackInfoArrayList;
    }
    // this is for displaying message if you have searched for empty field.
    public static boolean searchedForNone(String searchTerm){
        boolean searchedForNothing = false;
        if (searchTerm == ""){
            searchedForNothing = true;
        }
        return searchedForNothing;
    }
}