package DataAccess.demo.repositories;

import DataAccess.demo.models.Customer;
import DataAccess.demo.models.CustomerPerCountry;
import DataAccess.demo.models.CustomersFavoriteGenre;
import DataAccess.demo.models.HighestSpenders;
import DataAccess.demo.log.log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class CustomerRepository {

    private static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private static Connection conn = null;

    /**
     * Select all customers
     * @return all customers
     */
    public static ArrayList<Customer> selectAllCustomers() {

        ArrayList<Customer> customers = new ArrayList<>();

        try {
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");
            //Make SQL Query
            PreparedStatement ps = conn.prepareStatement("SELECT  CustomerId, FirstName, LastName, Email," +
                    "PostalCode, Phone, Country FROM Customer");

            //Execute Query
            //generateCustomer code block is repeated trough-out queries.
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                customers.add(generateCustomer(resultSet));
            }
            log.print("Successfully selected all customers!");

        } catch (Exception e) {
            log.print(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
        return customers;
    }

    /**
     * Returns a specific customer
     * @param customerId - customerId
     * @return - a specific customer's info
     */
    public static Customer selectCustomerById(String customerId) {
        Customer customer = null;
        try {
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");

            //Make SQL Query
            PreparedStatement ps = conn.prepareStatement("SELECT  CustomerId, FirstName, LastName, Email," +
                    "PostalCode, Phone, Country FROM Customer WHERE CustomerId= ?");
            ps.setString(1, customerId);

            //Execute Query
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                customer = generateCustomer(resultSet);
            }
            log.print("Successfully selected customer by ID");

        } catch (Exception e) {
            log.print(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
        return customer;
    }

    /**
     * Selects customer by first name and last name
     * @param firstName - customer first name
     * @param lastName  - customer last name
     * @return that specific customer info.
     */
    public static Customer selectCustomerByName(String firstName, String lastName) {
        Customer customer = null;
        try {
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");

            //Make SQL Query
            PreparedStatement ps = conn.prepareStatement("SELECT  CustomerId, FirstName, LastName, Email, " +
                    "PostalCode, Phone, Country, FROM Customer, WHERE FirstName LIKE ? AND LastName LIKE ? ");

            ps.setString(1, "%" + firstName + "%");
            ps.setString(2, "%" + lastName + "%");

            //Execute Query
            // ok the while loop is redundant here, or am I stupid? there will be one row?
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                customer = generateCustomer(resultSet);
            }
            log.print("Successfully selected customer by name!");


        } catch (Exception e) {
            log.print(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
        return customer;
    }


    /**
     * selects customers by limit and offset
     * @param limit  - the number of customers to be displayed
     * @param offset - offset
     * @return a customer list based on limit and offset
     */
    public static ArrayList<Customer> selectCustomersByOffsetAndLimit(int limit, int offset) {

        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");

            PreparedStatement ps = conn.prepareStatement(" SELECT CustomerId, FirstName, LastName, Email," +
                    "PostalCode, Phone, Country, FROM Customer LIMIT ? OFFSET ?");

            ps.setInt(1, limit);
            ps.setInt(2, offset);

            //Execute Query
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                customers.add(generateCustomer(resultSet));
            }
            log.print("Successfully selected customer by offset and limit!");

        } catch (Exception e) {
            log.print(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
        return customers;
    }

    // generating customer, since it is used multiple places above:
    public static Customer generateCustomer(ResultSet resultSet) throws Exception {
        return new Customer(
                resultSet.getString("CustomerId"),
                resultSet.getString("FirstName"),
                resultSet.getString("LastName"),
                resultSet.getString("Country"),
                resultSet.getString("PostalCode"),
                resultSet.getString("Phone"),
                resultSet.getString("Email")
        );
    }

    /**
     * Adds a new customer to the database
     *
     * @param customer
     * @return the newly added customer
     */
    public static Boolean addCustomer(Customer customer) {
        boolean success = false;
        try {
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");

            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO Customer " +
                    "(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);// if
            log.print("Add went well");

        } catch (Exception e) {
           log.print(e.toString());

        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }

        return success;
    }


    /**
     * Select customer by country
     * @return customers by country
     */
    public static ArrayList<CustomerPerCountry> selectCustomersByCountry() {
        ArrayList<CustomerPerCountry> customersByCountry = new ArrayList<>();
        try {
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");
            //Make SQL Query
            PreparedStatement ps = conn.prepareStatement("SELECT  COUNT(CustomerId) AS Customers, Country," +
                    "FROM Customer, GROUP BY Country, ORDER BY COUNT(CustomerId) DESC ");

            //Execute Query
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                customersByCountry.add(
                        new CustomerPerCountry(
                                resultSet.getInt("Customers"),
                                resultSet.getString("Country")
                        )
                );
            }
            log.print("Successfully selected customer by country");

        } catch (Exception e) {
            log.print(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
        return customersByCountry;
    }

    /**
     * updated an existing customer
     *
     * @param id             - customer id
     * @param updateCustomer - the updated customer
     */
    public static void updateCustomer(String id, Customer updateCustomer) {
        Customer existingCustomer = selectCustomerById(id);

        try {
            conn = DriverManager.getConnection(URL);
            log.print("Connection to Database has been established");

            // Make SQL Query
            PreparedStatement ps = conn.prepareStatement("UPDATE Customer" +
                    "SET FirstName=?, LastName=?, Country=?, PostalCode=?, Phone=?, Email=?, WHERE CustomerId= ?");

            ps.setString(1, updateCustomer.getFirstName() == null || updateCustomer.getFirstName().isEmpty()
                    ? existingCustomer.getFirstName() : updateCustomer.getFirstName());

            ps.setString(2, updateCustomer.getLastName() == null || updateCustomer.getLastName().isEmpty()
                    ? existingCustomer.getLastName() : updateCustomer.getLastName());

            ps.setString(3, updateCustomer.getCountry() == null || updateCustomer.getCountry().isEmpty()
                    ? existingCustomer.getCountry() : updateCustomer.getCountry());

            ps.setString(4, updateCustomer.getPostalCode() == null || updateCustomer.getPostalCode().isEmpty()
                    ? existingCustomer.getPostalCode() : updateCustomer.getPostalCode());

            ps.setString(5, updateCustomer.getPhone() == null || updateCustomer.getPhone().isEmpty()
                    ? existingCustomer.getPhone() : updateCustomer.getPhone());

            ps.setString(6, updateCustomer.getEmail() == null || updateCustomer.getEmail().isEmpty()
                    ? existingCustomer.getEmail() : updateCustomer.getEmail());

            ps.setString(7, id);
            ps.executeUpdate();
            log.print("Successfully updated!");

        } catch (Exception e) {
            log.print(e.toString());

        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
    }

    /**
     * select customers who paid highest
     *
     * @return highest spenders
     */
    public static ArrayList<HighestSpenders> selectHighestSpenders() {
        ArrayList<HighestSpenders> highestSpenders = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");

            PreparedStatement ps = conn.prepareStatement("SELECT C.CustomerId, C.FirstName, C.LastName," +
                    "SUM(TOTAL) AS Total " +
                    "FROM Customer C " +
                    "JOIN Invoice I on C.CustomerId = I.CustomerId " +
                    "GROUP BY I.CustomerId " +
                    "ORDER BY Total DESC LIMIT 10");

            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                highestSpenders.add(
                        new HighestSpenders(
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getInt("Total")
                        )
                );
            }
        } catch (Exception e) {
            log.print(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
        return highestSpenders;
    }

    /**
     * Select the most popular genre of a specific customer
     *
     * @param id - customer id
     * @return info about the most poplar genre of that customer
     */
    public ArrayList<CustomersFavoriteGenre> selectFavoriteGenre(String id) {
        ArrayList<CustomersFavoriteGenre> customersFavoriteGenre = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            log.print("Connection to SQLite has been established");

            PreparedStatement ps = conn.prepareStatement("WITH CustomersMostPopularGenre AS (\n" +
                    "    SELECT Cu.FirstName, Cu.LastName, Ge.Name AS GenreType, COUNT(Ge.GenreId) AS GenreCount\n" +
                    "    FROM Customer Cu\n" +
                    "             INNER JOIN Invoice Inv ON Cu.CustomerId = Inv.CustomerId\n" +
                    "             INNER JOIN InvoiceLine Il ON Inv.InvoiceId = Il.InvoiceId\n" +
                    "             INNER JOIN Track Tr ON Tr.TrackId = Il.TrackId\n" +
                    "             INNER JOIN Genre Ge ON Ge.GenreId = Tr.GenreId\n" +
                    "    WHERE Cu.CustomerId = ?\n" +
                    "    GROUP BY Ge.GenreId)\n" +
                    "\n" +
                    "SELECT CustomersMostPopularGenre.*\n" +
                    "FROM CustomersMostPopularGenre\n" +
                    "WHERE GenreCount = (SELECT MAX(GenreCount) FROM CustomersMostPopularGenre)");

            ps.setString(1, id);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                customersFavoriteGenre.add(
                        new CustomersFavoriteGenre(
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("GenreType"),
                                resultSet.getInt("GenreCount")
                        )
                );
            }
        } catch (Exception e) {
            log.print(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                log.print(e.toString());
            }
        }
        return customersFavoriteGenre;

    }

}
